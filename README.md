This project contains files that explains the basic demo on CSS that can be applied to an html page.

This project was only for the process of explaining the basics of CSS to give a basic idea of :
 - what CSS does
 - why we use it
 - how to use it
 - different types of CSS
 - basic syntax
 - different properties and values associated etc.